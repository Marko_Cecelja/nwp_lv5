<?php

return [
    'id' => 'ID',
    'croatian_title' => 'Naziv rada',
    'english_title' => 'Naziv rada na engleskom',
    'description' => 'Zadatak rada',
    'study_type' => 'Tip studija',
    'teacher' => 'Nastavnik',
    "tasks" => "Radovi",
    "administration" => "Administracija",
    "logout" => "Odjava",
    "newTask" => "Novi rad",
    "professional" => "Stručni",
    "undergraduate" => "Preddiplomski",
    "graduate" => "Diplomski",
    "submit" => "Spremi",
    "applyTask" => "Prijavi",
    "taskApplications" => "Prijavljeni radovi",
    "approve" => "Odobri",
    "denied" => "Obriši"
];
