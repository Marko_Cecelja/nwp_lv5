<?php

return [
    'id' => 'ID',
    'croatian_title' => 'Croatian task title',
    'english_title' => 'English task title',
    'description' => 'Description',
    'study_type' => 'Study type',
    'teacher' => 'Teacher',
    "tasks" => "Tasks",
    "administration" => "Administration",
    "logout" => "Logout",
    "newTask" => "New task",
    "professional" => "Professional",
    "undergraduate" => "Undergraduate",
    "graduate" => "Graduate",
    "submit" => "Submit",
    "applyTask" => "Apply",
    "taskApplications" => "Applications",
    "approve" => "Approve",
    "denied" => "Denied"
];
