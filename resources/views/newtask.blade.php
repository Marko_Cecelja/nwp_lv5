@extends('layouts.app')

@section('content')
    <!DOCTYPE html>
<html lang="en">
<head>
    <title>New project</title>
</head>
<body>
<form class="container col-md-4 col-md-offset-4" method="POST" action="{{ route('tasks') }}">
    @csrf
    <div class="form-group">
        <label for="croatian_title">{{ __('messages.croatian_title') }}: </label>
        <input class="form-control" type="text" name="croatian_title" id="croatian_title" required>
    </div>
    <div class="form-group">
        <label for="english_title">{{ __('messages.english_title') }}: </label>
        <input class="form-control" type="text" name="english_title" id="english_title" required>
    </div>
    <div class="form-group">
        <label for="description">{{ __('messages.description') }}: </label>
        <textarea class="form-control" rows=5 name="description" id="description" required></textarea>
    </div>
    <div class="form-group">
        <select name="study_type" required id="study_type">
            <option value="" disabled selected>{{ __('messages.study_type') }}</option>
            <option value="1">{{ __('messages.professional') }}</option>
            <option value="2">{{ __('messages.undergraduate') }}</option>
            <option value="3">{{ __('messages.graduate') }}</option>
        </select>
    </div>
    <button type="submit" class="btn btn-primary">{{ __('messages.submit') }}</button>
</form>
</body>
</html>
@endsection
