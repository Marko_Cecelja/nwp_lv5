@extends('layouts.app')
@section('content')
    <!DOCTYPE html>
<html lang="en">
<head>
    <title>All projects</title>
    <style>
        .table {
            margin-top: 10%;
        }
    </style>
</head>
<body>
<div class="container table-responsive">
    <table class="table table-striped table-hover">
        <thead class="thead-dark">
        <th>{{ __('messages.id') }}</th>
        <th>{{ __('messages.croatian_title') }}</th>
        <th>{{ __('messages.english_title') }}</th>
        <th>{{ __('messages.description') }}</th>
        <th>{{ __('messages.study_type') }}</th>
        <th></th>
        <th></th>
        </thead>
        <tbody>
        @foreach ($tasks as $task)
            <tr>
                <td>
                    <div>{{$task->id}}</div>
                </td>
                <td>
                    <div>{{$task->croatian_title}}</div>
                </td>
                <td>
                    <div>{{$task->english_title}}</div>
                </td>
                <td>
                    <div>{{$task->description}}</div>
                </td>
                <td>
                    <div>{{ __('messages.'.strtolower($task->studyType->name)) }}</div>
                </td>
                @if($task->userTasks[0]->is_approved == false)
                    <td>
                        <form id="approve-task-{{$task->id}}-form" action="{{ route('approve', $task->id)}}"
                              method="POST">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-primary">{{ __('messages.approve') }}</button>
                        </form>
                    </td>
                    <td>
                        <form id="denied-task-{{$task->id}}-form" action="{{ route('denied', $task->id)}}"
                              method="POST">
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-primary">{{ __('messages.denied') }}</button>
                        </form>
                    </td>
                @else
                    <td>
                    </td>
                    <td>
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
</body>
</html>
@endsection
