@extends('layouts.app')
@section('content')
    <!DOCTYPE html>
<html lang="en">
<head>
    <title>All projects</title>
    <style>
        .table {
            margin-top: 10%;
        }
    </style>
</head>
<body>
<div class="container table-responsive">
    <table class="table table-striped table-hover">
        <thead class="thead-dark">
        <th>{{ __('messages.id') }}</th>
        <th>{{ __('messages.croatian_title') }}</th>
        <th>{{ __('messages.english_title') }}</th>
        <th>{{ __('messages.description') }}</th>
        <th>{{ __('messages.study_type') }}</th>
        <th>{{ __('messages.teacher') }}</th>
        <th></th>
        @php($user = Auth::user())
        </thead>
        <tbody>
        @foreach ($tasks as $task)
            <tr>
                <td>
                    <div>{{$task->id}}</div>
                </td>
                <td>
                    <div>{{$task->croatian_title}}</div>
                </td>
                <td>
                    <div>{{$task->english_title}}</div>
                </td>
                <td>
                    <div>{{$task->description}}</div>
                </td>
                <td>
                    <div>{{ __('messages.'.strtolower($task->studyType->name)) }}</div>
                </td>
                <td>
                    <div>{{$task->createdBy->email}}</div>
                </td>
                <td>
                    @if($user->role != null && $user->role->id == 3)
                        <form id="apply-task-{{$task->id}}-form" action="{{ route('applyTask', $task->id)}}"
                              method="POST">
                            {{ csrf_field() }}
                            <button @if(count($user->userTasks) > 0) disabled @endif type="submit"
                                    class="btn btn-primary">{{ __('messages.applyTask') }}</button>
                        </form>
                    @else
                        <div></div>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @if($user->role != null && $user->role->id == 2)
        <a class="btn btn-xs btn-info pull-right" href="{{ route('newTask') }}">{{ __('messages.newTask') }}</a>
    @endif
</div>
</body>
</html>
@endsection
