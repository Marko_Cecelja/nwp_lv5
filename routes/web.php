<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/administrations', 'App\Http\Controllers\AdministrationController@getAdministrationView')->name('administrations');

Route::group(['prefix' => 'users'], function () {
    Route::put('/{user_id}/role', 'App\Http\Controllers\UserController@updateUserRole')->name('updateRole');
    Route::post('/add', 'App\Http\Controllers\UserController@addUser')->name('adduser');
});

Route::group(['prefix' => 'tasks'], function () {
    Route::get('/', 'App\Http\Controllers\TaskController@getTasksView')->name('tasks');
    Route::get('/new', 'App\Http\Controllers\TaskController@getNewTaskView')->name('newTask');
    Route::post('/', 'App\Http\Controllers\TaskController@saveNewTask')->name('tasks');
    Route::post('/{task_id}/apply', 'App\Http\Controllers\TaskController@applyForTask')->name('applyTask');
    Route::get('/applications', 'App\Http\Controllers\TaskController@genPendingApprovalTasks')->name('applications');
    Route::post('/{task_id}/approve', 'App\Http\Controllers\TaskController@approve')->name('approve');
    Route::post('/{task_id}/denied', 'App\Http\Controllers\TaskController@denied')->name('denied');
});

Route::post('/changeLanguage', 'App\Http\Controllers\LanguageController@changeLanguage')->name('changeLanguage');
