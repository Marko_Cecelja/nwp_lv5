<?php

namespace App\Repositories;

use App\Models\Task;
use Illuminate\Support\Facades\DB;

class TaskRepository
{
    public function getTasksWithoutUsers()
    {
        return Task::whereNotIn('id', function($query){
            $query->select('task_id')
                ->from('user_tasks');
        })->get();
    }

    public function getTasksCreatedByUserAndWaitingApproval($user)
    {
        return Task::whereIn('id', function($query){
            $query->select('task_id')
                ->from('user_tasks');
        })->where('created_by_id', '=', $user->id)->get();
    }
}
