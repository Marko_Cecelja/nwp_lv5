<?php

namespace App\Http\Controllers;

use App\Models\User;

class AdministrationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    public function getAdministrationView()
    {
        $nonAdminUsers = User::where('role_id', '=', null)->orderBy('created_at')->get();
        return view('administration', [
            'users' => $nonAdminUsers
        ]);
    }
}
