<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Models\UserTask;
use App\Repositories\TaskRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{

    protected $taskRepository;

    public function __construct(TaskRepository $taskRepository)
    {
        $this->middleware('auth');
        $this->middleware('teacher')->only(['genPendingApprovalTasks']);
        $this->taskRepository = $taskRepository;
    }

    public function getTasksView()
    {
        $user = Auth::user();
        $tasks = [];
        if ($user->role->id == 2) {
            $tasks = Task::where("created_by_id", "=", $user->getId())->orderBy('created_at')->get();
        } else if ($user->role->id == 3) {
            $tasks = $this->taskRepository->getTasksWithoutUsers();
        } else {
            $tasks = Task::orderBy('created_at')->get();
        }

        return view('tasks', [
            'tasks' => $tasks
        ]);
    }

    public function getNewTaskView()
    {
        return view('newtask');
    }

    public function saveNewTask(Request $request)
    {
        $task = new Task();
        $task->croatian_title = $request->croatian_title;
        $task->english_title = $request->english_title;
        $task->description = $request->description;
        $task->study_type_id = $request->study_type;

        $task->created_by_id = Auth::user()->getId();;

        $task->save();

        return redirect("/tasks");
    }

    public function applyForTask($task_id)
    {
        $user = Auth::user();

        $userTask = new UserTask();
        $userTask->task_id = $task_id;
        $userTask->user_id = $user->id;
        $userTask->is_approved = false;

        $userTask->save();

        return redirect("/tasks");
    }

    public function genPendingApprovalTasks()
    {
        $user = Auth::user();

        $tasks = $this->taskRepository->getTasksCreatedByUserAndWaitingApproval($user);

        return view('tasksApplications', [
            'tasks' => $tasks
        ]);
    }

    public function approve($task_id)
    {
        $userTask = UserTask::where("task_id", "=", $task_id)->update(['is_approved' => true]);

        return redirect("/tasks/applications");
    }

    public function denied($task_id)
    {
        UserTask::where('task_id', $task_id)->delete();

        return redirect("/tasks/applications");
    }
}
