<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = ['croatian_title', 'english_title', 'description'];

    public function studyType() {
        return $this->belongsTo(StudyType::class);
    }

    public function createdBy() {
        return $this->belongsTo(User::class);
    }

    public function userTasks() {
        return $this->hasMany(UserTask::class);
    }
}
