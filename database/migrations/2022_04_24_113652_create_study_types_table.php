<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('study_types', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->timestamps();
        });

        $studyTypes = [
            ['name'=>'PROFESSIONAL', 'created_at'=> now()],
            ['name'=>'UNDERGRADUATE', 'created_at'=> now()],
            ['name'=>'GRADUATE', 'created_at'=> now()]
        ];

        DB::table('study_types')->insert($studyTypes);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('study_types');
    }
};
